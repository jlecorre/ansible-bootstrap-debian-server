#
# this vagrantfile is used to test ansible deployment in virtual machine
# distribution used in this vagrant box : debian/buster64 (aka. Debian Buster / Debian10)
#
Vagrant.require_version ">= 2.0.0"

# define vagrant configuration
Vagrant.configure(2) do |config|

  # vagrant box to use for testing deployment with ansible
  config.vm.box = "debian/buster64"
  # disable the new default behavior introduced in Vagrant 1.7, to
  # ensure that all Vagrant machines will use the same SSH key pair.
  # see https://github.com/mitchellh/vagrant/issues/5005
  config.ssh.insert_key = false
  # set network configuration to reuse in inventory/hosts
  config.vm.network :private_network, ip: "192.168.110.112"
  # set machine hostname
  config.vm.hostname = "ansible-deploy-buster"

  # specify a VM provider (Virtualbox, Docker, etc.)
  config.vm.provider "virtualbox" do |vb|
    # customize the amount of memory on the VM
    vb.memory = "512"
    # customize the amount of CPUs on the VM
    vb.cpus = "2"
    # display the VirtualBox GUI when booting the machine
    vb.gui = false
  end

  # execute shell command
  config.vm.provision "shell", inline: <<-SHELL
    mkdir -p "/srv"
    # set custom passwords in vagrant box
    echo "vagrant:vagrant" | sudo chpasswd
  SHELL

  # define ansible configuration
  config.vm.provision "ansible" do |ansible|
    # enable verbose mode during playbook execution
    ansible.verbose = "v"
    # path to playbook to execute
    ansible.playbook = "../../bootstrap_debian_server.yml"
    # if you need to test only a part of your playbook, you can declare some tag(s)s
    # ansible.tags = "apt,docker"
    # if you want to skip tag(s) during execution of Ansible playbook
    ansible.skip_tags = "ssh"
    # become sudo to execute tasks with root privileges
    ansible.become = true
    ansible.become_user = "root"
    # require Ansible to prompt for a password
    ansible.ask_become_pass = true
    # compatibility of the generated Ansible inventory
    ansible.compatibility_mode = "auto"
    # useful to promt to user passphrase to decrypt vault files
    ansible.ask_vault_pass = false
    # path of a file containing the password used by Ansible Vault
    # /!\ be careful to not add this file in your git repository /!\
    ansible.vault_password_file = "../vault.password"
    # pass additional variables to the playbook
    ansible.extra_vars = {
      host_to_deploy: "default"
    }
  end

end
