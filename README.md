# Playbook de bootstrap d'un serveur Debian

Ce dépôt contient un playbook Ansible permettant de bootstraper un serveur Debian après une installation "from scratch"  

## Sommaire

* [Pré-requis](#pré-requis)
* [Dépendances](#dépendances)
* [Liste des machines disponibles](#liste-des-machines-disponibles)
* [Fonctionnement du playbook](#fonctionnement-du-playbook)
  * [Liste et description des tâches](#liste-et-description-des-tâches-qui-peuvent-être-exécutées)
  * [Liste des tags disponibles](#liste-des-tags-disponibles)
  * [Comment lister les tags disponibles avec Ansible ?!](#comment-lister-les-tags-disponibles-avec-ansible-)
  * [Vérification de la syntaxe du playbook](#vérification-de-la-syntaxe-du-playbook)
  * [Exécution du playbook](#exécution-du-playbook)
* [Troubleshooting (a.k.a gestion des erreurs)](#troubleshooting-aka-gestion-des-erreurs)
* [Informations importantes concernant la tâche ssh](#informations-importantes-concernant-la-tâche-ssh)
* [Connexion SSH après durcissement de la configuration](#connexion-ssh-après-durcissement-de-la-configuration)
* [Structure des données chiffrées](#structure-des-données-chiffrées)
* [Tests du playbook](#tests-du-playbook)
* [Informations supplémentaires](#informations-supplémentaires)
* [Informations concernant l'auteur](#informations-concernant-lauteur)

## Pré-requis

Pour utiliser ce playbook, il faut :

* Le mot de passe de l'utilisateur qui exécutera les tâches Ansible sur la machine cible ou sa clef SSH
* Un serveur Debian fraichement installé

**Matrice des compatibilités :**

Systèmes / Applicatifs | Versions testées
-----------------------|-----------------
Debian GNU/Linux       | >= 8.7 (jessie)
Debian GNU/Linux       | >= 9.0 (stretch)
Debian GNU/Linux       | >= 10.0 (buster)
Ansible                | >= 2.7.0.0
Python                 | >= 2.7.12
Vagrant                | >= 2.0.0

## Dépendances

* Pas de dépendance aux rôles Ansible actuellement.  
* Il est prévu à terme de factoriser le code et de basculer vers une utilisation des rôles Ansible.  
* Dépendances à un ou plusieurs dépôt(s) Git utilisé(s) pour la configuration de la distribution.

## Liste des machines disponibles

Se référer à la liste présente dans l'inventory

```shell
└── inventory
    └── hosts
```

## Fonctionnement du playbook

Ce playbook Ansible est là pour simplifier/faciliter le bootstrap d'une machine fraichement livrée par votre fournisseur.  
Le tout en suivant le principe suivant : ```une ligne de commande = une distribution fonctionnelle et personnalisée```

### Liste et description des tâches qui peuvent être exécutées

* Créer les utilisateurs et les groupes déclarés dans le fichier ```group_vars/users.yml```  
*(attention le fichier est chiffré dans le dépôt, il faut créer le votre, voir un extrait du fichier plus bas)*
* Regénérer la configuration des locales de la distribution
* Personnaliser la zone de temps de la distribution
* Personnaliser les sources.list de la distribution
* Mettre à jour le cache APT
* Supprimer les paquets systèmes déclarés comme inutile dans ```group_vars/package_list.yml```
* Installer les paquets systèmes déclarés comme nécessaires dans ```group_vars/package_list.yml```
* Installer et configurer les services Docker et Docker-Compose (gestion des utilisateurs et groupes pris en compte)  
* Cloner le dépôt GitLab utilisé pour les tâches de "personnalisation" de la distribution : [sysadmin-tools repository](https://gitlab.com/jlecorre/sysadmin-tools)
* Générer une configuration personnalisée du système :
  * Configurer Bash et son prompt (a.k.a déploiement d'un fichier `.bashrc`)  
  * Personnaliser Vim (a.k.a déploiement d'un fichier `.vimrc`)  
  * Personnaliser Git (a.k.a déploiement des fichiers `.gitignore` et `.gitconfig`)  
  * Personnaliser la configuration de Screen (a.k.a déploiement d'un fichier `.screenrc`)  
  * Personnaliser la configuration de l'outil `yamllint` (a.k.a déploiement d'un fichier `.yamllint.cfg`)  
  * Etc...
* Déployer le script de MOTD dynamique hébergé ici : [dépôt sysadmin-tools via GitLab](https://gitlab.com/jlecorre/sysadmin-tools)  
* Générer les alias du système  
* Déployer une configuration avancée de Fail2ban et démarrer le service  
  > @TODO - Extraire cette tâche du playbook pour en faire un rôle indépendant  
* Installer un environnement LAMP (Linux|Apache|MySQL|MariaDB|PHP)  
* Sécuriser la configuration du système en modifiant le fichier ```/etc/sysctl.conf```  
* Installer et configurer le programme LogWatch  
* Installer et configurer le programme Rkhunter  
* Installer et configurer le programme Apticron  
* Déployer une configuration "hardened" du serveur OpenSSH  
/!\ __WARNING__ - merci de lire le paragraphe [Informations importantes concernant la tâche ssh](#informations-importantes-concernant-la-tâche-ssh) avant de l'exécuter /!\
* Plus si affinitée ... et les contributions sont les bienvenues =)

### Liste des tags disponibles

En utilisant le système des "tags" intégré à Ansible, il est possible de n'exécuter que les "tasks" qui vous intéressent !  
Pour les utiliser, voir la commande décrite au paragraphe "Exécution du playbook".

#### Lister les tags disponibles avec Ansible

```shell
ansible-playbook bootstrap_debian_server.yml --list-tags
```

Liste des tags disponibles :
> aliases  
> apt  
> apticron  
> bash  
> docker  
> f2b  
> git  
> lamp  
> locales  
> logwatch  
> motd  
> rkhunter  
> ssh *(merci de lire le paragraphe [Informations importantes concernant la tâche ssh](#informations-importantes-concernant-la-tâche-ssh) avant de l'exécuter)*  
> sysctl  
> timezone  
> users  
> vim  

### Vérification de la syntaxe du playbook

Pour lancer une analyse de la syntaxe des fichiers YAML composant le playbook :

```shell
ansible-playbook -i inventory/hosts --syntax-check bootstrap_debian_server.yml
```

### Exécution du playbook

```shell
ansible-playbook -i inventory/hosts bootstrap_debian_server.yml \
  -e "host_to_deploy=${group_of_hosts}" \
  -e "authorized_user=${authorized_ssh_username}" \
  [--skip-tags "ssh"] \
  [--tags "${your_tags}"] \
  [--skip-tags "${your_tags}"] \
  [--ask-pass] \
  [--ask-become-pass] \
  [--vault-password-file /path/to/passphrase] \
  [--ask-vault-pass] \
  [-vvv] \
  [-e "ansible_python_interpreter=/usr/bin/python3"]
  [--private-key=/path/to/private/key]
  [--user=${username}]
```

Avec les paramètres suivants :  

Nom du paramètre                          | Description
------------------------------------------|-----------------------------------------------------------------------------------------
-e "host_to_deploy=${group_of_hosts}"     | Le groupe de serveurs à bootstraper
-e "authorized_user=${authorized_ssh_username}" | L'utilisateur SSH autorisé à se connecter au serveur à déployer
--tags "${your_tags}"                     | La liste des tags à exécuter
--skip-tags "${your_tags}"                | Liste des tags à ne pas jouer lors de l'exécution du playbook *(valeur par défaut : ssh)*
--ask-pass                                | Paramètre à utiliser lorsque l'on ne dispose pas de clef SSH
--ask-become-pass                         | Le mot de passe de l'utilisateur root pour exécuter le déploiement après s'être connecté en SSH avec un utilisateur autorisé et non privilégié
--vault-password-file /path/to/passphrase | Permet de ne pas renseigner la passphrase Vault manuellement
--ask-vault-pass                          | Permet de renseigner la passphrase Vault manuellement à l'exécution du playbook
-vvv                                      | Permet d'activer le mode "verbeux" d'Ansible
-e "ansible_python_interpreter=/usr/bin/python3" | Permet de spécifier l'interpréteur Python à utiliser sur la machine distante
--private-key=/path/to/private/key        | Permet une clef ssh privée spécifique pour initier une connexion sur la machine distante *(voir la task ssh hardening)*
--user=${username}                        | Permet de spécifier l'utilisateur ayant l'autorisation d'initier des connexions SSH sur la machine distante *(voir la task ssh hardening)*

#### Exemple d'exécution du playbook

* Lorsque le serveur __n'a pas encore__ été bootstrapé :  

```shell
ansible-playbook -i inventory/hosts bootstrap_debian_server.yml \
  -e "authorized_user=root" -e "host_to_deploy=bar" \
  --ask-vault-pass --ask-pass --skip-tags "ssh"
```

* Lorsque le serveur __est déjà__  bootstrapé et que la tâche Ansible __SSH__ a été exécutée :  

```shell
ansible-playbook -i inventory/hosts bootstrap_debian_server.yml \
  -e "authorized_user=foo" -e "host_to_deploy=bar" --ask-become-pass \
  --ask-vault-pass --private-key=${HOME}/.ssh/ssh_key_ed25519
```

#### Troubleshooting (a.k.a gestion des erreurs)

##### Mauvais interpréteur Python

```shell
fatal: [hostname]: FAILED! =>
{
  "changed": false,
  "module_stderr": "Shared connection to hostname closed.\r\n",
  "module_stdout": "/bin/sh: /usr/bin/python: No such file or directory\r\n",
  "msg": "MODULE FAILURE",
  "rc": 0
}
```

Si l'erreur suivante apparaît lors de la première exécution du playbook, deux solutions s'offrent à vous !  

Installer Python version __`2.X`__ sur la machine distante :  

```shell
=> Via Ansible
ansible ${targeted_host} -m raw -a "apt install -y python2 python-simplejson"

=> Ou directement en ligne de commande
apt install -y python2 python-simplejson
```

Utiliser l'interpréteur Python version __`3.X`__ :  

```shell
=> Via les extra_vars de la commande ansible-playbook
-e 'ansible_python_interpreter=/usr/bin/python3'

=> Ou en ajoutant le paramètre suivant directement dans votre fichier inventory
ansible_python_interpreter=/usr/bin/python3
```

#### Informations importantes concernant la tâche SSH

Le playbook `bootstrap_debian_server.yml` propose l'utilisation du tag `ssh`.  
Veuillez lire le code source de la tâche `tasks/deploy_ssh_configuration.yml` avant de l'utiliser, cette tâche déploie une configuration stricte du serveur OpenSSH.  
Je vous conseille fortement l'utilisation de l'option suivante lors de l'exécution du playbook `--skip-tags ssh` si vous ne savez pas ce que vous faites !  
Selon la configuration de votre client SSH, il existe un risque __non négligeable__ de perdre l'accès SSH au serveur fraîchement déployé.  

Voici une liste exhaustive des actions réalisées spécifiquement par la tâche `deploy_ssh_configuration.yml` :  

* Mise à jour du fichier `${HOME}/.ssh/authorized_keys`
* Suppression de __TOUTES__ les clefs SSH générées automatiquement lors de l'installation du serveur OpenSSH
* Génération d'une nouvelle clef SSH utilisant l'algorithme de signature __ed25519__  
*(Se référer aux articles [EdDSA - schéma de signature numérique](https://fr.wikipedia.org/wiki/EdDSA) et [EcDSA - algorithme de signature numérique à clef publique](https://fr.wikipedia.org/wiki/Elliptic_curve_digital_signature_algorithm) sur Wikipédia pour plus d'informations)*
* Création d'un groupe d'utilisateurs qui seront les seuls à être autorisés à initier une connexion SSH
* Durcissement de la configuration `/etc/ssh/ssh_config` via un template Jinja2
* Durcissement de la configuration `/etc/ssh/sshd_config` via un template Jinja2
* Redémarrage du serveur OpenSSH

L'exécution de cette tâche Ansible est contrainte aux pré-requis suivants :  

* Exécution unique  

```text
Le fichier `/opt/server-already-bootstraped` est déployé lors de la première exécution de la task ssh
Il servira de marqueur permettant de ne pas réexécuter les tasks du playbook deploy_ssh_configuration.yml
Pour rejouer ce playbook, il vous suffira de supprimer le fichier concerné
```

* Certaines tâches du playbook ne seront exécutées uniquement sur les versions `>= stretch` de la distribution Debian
* Le fichier de configuration `group_vars/all/ssh_config.yml` et `group_vars/all/users.yml` doivent être mis à jour pour répondre à vos besoins

Pour vérifier le statut de la configuration de votre serveur SSH, je vous conseille l'utilisation du site __CryptCheck__ gracieusement mis à disposition par [@aeris](https://imirhil.fr/).
C'est donc par ici que cela se passe : [SSL Check](https://tls.imirhil.fr/ssh)  

*Sources utilisée pour la mise en place de cette configuration :*

* [Secure shell mon amour déchu](https://blog.arnaudminable.net/secure-shell-mon-amour-dechu/)
* [Secure shell](https://stribika.github.io/2015/01/04/secure-secure-shell.html)

#### Connexion SSH après durcissement de la configuration

##### Utilisation du fichier Vagantfile

Après exécution du playbook via le fichier `Vagrantfile` avec le tag `ssh` actif, l'utilisation de l'outil Vagrant n'est plus possible pour les options `vagrant ssh` et `vagrant provision`.  

Liste d'erreurs rencontrées :

```text
Permission denied (publickey)
could not settle on host_key algorithm
```

Pour se connecter à la machine virtuelle fraîchement générée, il faut utiliser la commande suivante :  

```shell
ssh -i /path/to/your/ssh/private/key -p ${ssh_port:=22} ${vm_ip_address:=192.168.110.111}
```

Pour exécuter le playbook via `vagrant` sans appliquer la configuration *durcie* du serveur SSH, voir l'attribut `ansible.skip_tags` dans le fichier `Vagrantfile`.  

##### Connexion aux machines de production

Lorsque le playbook est exécuté avec le tag `ssh` permettant le durcissement de la configuration du serveur SSH, les connexions SSH aux serveurs bootstrapés sont autorisées avec les conditions suivantes :  

* L'utilisation d'un mot de passe via SSH est __interdite__
* L'utilisation d'une clef SSH `ed25519` est __obligatoire__  
* Les connexions SSH avec l'utilisateur root sont __interdites__

Comment se connecter à sa machine :  

```shell
ssh -i /path/to/your/ssh/private/key -p ${ssh_port:=22} ${hostname}
```

###### Génération d'une clef SSH

Cette ligne de commande permet la génération d'une clef SSH `ed25519` qu'il est possible de déployer avec ce playbook.  
Attention de ne pas publier votre clef __privée__, seule la clef publique peut être __publique__.  

```shell
ssh-keygen -t ed25519 -o -a 1000 -C "your_comment" -b 4096 -f filename
```

### Structure des données chiffrées

Afin de *''sécuriser''* les serveurs déployés via ce playbook, certains fichiers disponibles dans ce dépôt de code sont chiffrés.  
Suite aux premiers feedbacks qui m'ont été fait et pour ne pas *''bloquer''* l'utilisation de ce playbook, voici les structures des fichiers et variables que vous aurez besoin de reproduire dans votre environnement de travail :

#### > group_vars/all/users.yml

Déclaration des utilisateurs/groupes système utilisés par les tâches suivantes du playbook :

* creating system groups  
* creating and configuring system users
* creating group of users allowed to initiate connection on OpenSSH server

```yaml
---
# list of system user to create during installation
system_users:
  - { name: "root", home: "/root", password: "root_password" }
  - { name: "user1", home: "/home/user1", password: "password1"}
  - { name: "user2", home: "/home/user2", password: "password2" }
  ...
  - { name: "userN", home: "/home/userN", password: "passwordN" }

# list of users authorized to initiate SSH connection
ssh_allow_users:
  - { name : "user1", key: "ssh-ed25519 content_of_the_public_ssh_key_of_the_user1" }
  - { name : "user2", key: "ssh-ed25519 content_of_the_public_ssh_key_of_the_user2" }
  ...
  - { name : "userN", key: "ssh-ed25519 content_of_the_public_ssh_key_of_the_userN" }
...
```

Pour générer des mots de passe chiffrés et les ajouter au fichier `group_vars/all/users.yml`, il faut utiliser la commande suivante dans un terminal :  

```shell
mkpasswd --method=sha-512
```

Entrer le mot de passe souhaité (il sera demandé interactivement), puis ajouter la sortie de la commande en tant que valeur de la clef `password` dans le fichier chifffré via `Vault`.  

__Attention à ne pas oublier le mot de passe généré !!__  
Pour stocker vos secrets dans de bonnes conditions, je vous conseille le __coffre fort numérique__ suivant : [KeePass](https://keepass.info/)  

#### > group_vars/all/gitlab_rsa_key.yml

Il est également possible de déclarer les clefs SSH nécessaires pour s'authentifier et cloner des dépôts GIT **privés** :

```yaml
---
# list of keys used to authenticated in services used by this playbook
gitlab_rsa_key: "ma_super_clef_ssh_rsa_or_whatever"
...
```

### Tests du playbook

Pour tester un playbook Ansible, il est possible d'utiliser [Vagrant](https://www.vagrantup.com/).  
Le répertoire ```tests``` contient le nécessaire pour démarrer une box Vagrant et exécuter le playbook Ansible en local.

```shell
.
├── debian10
│   └── Vagrantfile
├── debian8
│   └── Vagrantfile
└── debian9
    └── Vagrantfile
```

Pour simuler la création et la mise à jour des __utilisateurs système__ générés par Ansible, des utilisateurs sont créés *"manuellement"* lors de l'instanciation des machines virtuelles Vagrant.  

Lors de l'exécution du playbook via Vagrant, un mot de passe `sudo` est demandé, merci de vous référer au tableau suivant :  

Nom d'utilisateur | Mot de passe | Commentaires
------------------|--------------|-----------------------------
vagrant           | vagrant      | Utilisateur exécutant le playbook
root              | vagrant      | Utilisateur cible de l'instruction `become` d'Ansible

Commandes utiles à connaitre :  

```shell
# démarre une box Vagrant et exécute le playbook Ansible en local
# (voir le fichier inventory/hosts pour la configuration SSH d'Ansible)
vagrant up
# rejoue le playbook Ansible dans une box Vagrant déjà démarrée
vagrant provision
# supprime définitivement une box Vagrant
vagrant destroy
# permet de se connecter en SSH à une boxe démarrée (pratique pour aller voir ce qu'il se passe dedans :)
vagrant ssh
# ou encore
ssh -i ${HOME}/.vagrant.d/insecure_private_key vagrant@<ip_vagrant_box>
```

## Informations supplémentaires

* Si nécessaire, penser à configurer une clef SSH avant d'utiliser les tags qui exécutent un ```git clone``` sur un dépôt de code "protégé"
* Permissions nécessaire à la protection de la clef SSH : ```chmod 0600 file_name```

## Informations concernant l'auteur

Joël LE CORRE `<contact[@]sublimigeek.fr>`
