help: ## Show help message to user
	@ echo 'Usage: make [target] [VARIABLE]'
	@ echo ''
	@ echo '- To pull Markdownlint Docker image:                                          `make markdown-lint-installation`'
	@ echo '- To execute Markdown linter on all `*.md` files available in the repository: `make markdown-lint`'
	@ echo '- To execute Ansible linter on all available playbooks in the repository:     `make ansible-lint`'

# https://gitlab.com/06kellyjac/docker_markdownlint-cli
markdown-lint-installation:
	docker image pull 06kellyjac/markdownlint-cli:latest 

# List of existing rules: https://github.com/DavidAnson/markdownlint/blob/master/doc/Rules.md
markdown-lint:
	docker container run \
	--rm \
	-v ${PWD}:/markdown 06kellyjac/markdownlint-cli \
	-c .markdownlint.json \
	*.md

# List of existing rules: https://docs.ansible.com/ansible-lint/rules/default_rules.html
ansible-lint:
	ansible-lint -c .ansible-lint bootstrap_debian_server.yml